import React, { useRef, useState } from 'react';
import styled from 'styled-components';

import { Minus as MinusIcon, Plus as PlusIcon } from 'react-feather';

import AppHeader from '@components/layouts/AppHeader';
import Candlestick, { CandlestickDataType, CandlestickRef, CandlestickTimeSeries } from '@components/charts/Candlestick';
import { GlobalThemeOptions } from '@utils/global-theme';


declare const __WEBPACK_DEFINE__: {
  APPEND_COUNT: number;
  DEMO_DATA: { //
    minutely: CandlestickDataType[];
    hourly: CandlestickDataType[];
    daily: CandlestickDataType[];
    monthly: CandlestickDataType[];
  };
};


//* Components
const Container = styled('div')`
  ${({ theme }: { theme: GlobalThemeOptions; }) => `
    display: flex;
    flex-direction: column;
    align-items: center;
    height: 100%;
    overflow: hidden auto;
    padding: ${theme.spacing(1, 3)};
  `}
`;

const IconButton = styled('button')`
  ${({ theme }: { theme: GlobalThemeOptions; }) => `
    cursor: pointer;
    display: flex;
    align-items: center;
    justify-content: center;
    border: 1px solid ${theme.chart.stroke};
    width: ${theme.spacing(4)};
    height: ${theme.spacing(4)};
    border-radius: 50%;
  `}
`;

export default function HomePage() {
  const [timeSeries, setTimeSeries] = useState<CandlestickTimeSeries>('minutely');
  const ref = useRef<CandlestickRef>(null);

  return (
    <>
      <AppHeader />

      <Container>
        <Candlestick
          ref={ref}
          information={`${timeSeries} Prices (open, high, low, close) and Volumes`}
          symbol="MSFT"
          timeSeries={timeSeries}
          timeZone="US/Eastern"
          data={__WEBPACK_DEFINE__.DEMO_DATA[timeSeries]}
          onTimeSeriesChange={setTimeSeries}
          action={(
            <>
              <IconButton onClick={() => ref.current.setZoom('zoomin')}>
                <PlusIcon />
              </IconButton>

              <IconButton onClick={() => ref.current.setZoom('zoomout')}>
                <MinusIcon />
              </IconButton>
            </>
          )}
        />
      </Container>
    </>
  );
}
