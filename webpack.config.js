const HtmlWebpackPlugin = require('html-webpack-plugin');
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin');
const path = require('path');

const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { DefinePlugin } = require('webpack');
const generation = require('./webpack.data');

const dependencies = ['react', 'react-router-dom', 'styled-components'];

module.exports = ({ port }, { mode }) => ({
  mode,
  devtool: 'source-map',
  devServer: {
    compress: true,
    hot: true,
    port
  },
  entry: {
    ...(dependencies.reduce(
      (result, package) => ({ ...result, [package]: package }),
      {}
    )),
    index: {
      import: path.resolve(__dirname, 'src/index.tsx'),
      dependOn: dependencies
    }
  },
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'js/[name].js',
    chunkFilename: 'js/[id]_[hash].js'
  },
  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.tsx'],
    alias: {
      'clsx': path.resolve(__dirname, 'node_modules/clsx/'),
      'lodash': path.resolve(__dirname, 'node_modules/lodash/'),
      'prop-types': path.resolve(__dirname, 'node_modules/prop-types/'),
      'react': path.resolve(__dirname, 'node_modules/react/'),
      'react-dom': path.resolve(__dirname, 'node_modules/react-dom/'),
      'react-router-dom': path.resolve(__dirname, 'node_modules/react-router-dom/'),
      'styled-components': path.resolve(__dirname, 'node_modules/styled-components/'),

      '@assets': path.resolve(__dirname, './src/assets'),
      '@components': path.resolve(__dirname, './src/components'),
      '@pages': path.resolve(__dirname, './src/pages'),
      '@utils': path.resolve(__dirname, './src/utils')
    }
  },
  optimization: {
    runtimeChunk: 'single'
  },
  plugins: [
    ...(mode === 'production' ? [] : [new ReactRefreshWebpackPlugin()]),
    new CleanWebpackPlugin(),

    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './index.html')
    }),
    new DefinePlugin({
      '__WEBPACK_DEFINE__.APPEND_COUNT': JSON.stringify(50),
      '__WEBPACK_DEFINE__.DEFAULT_PADDING_LEFT': JSON.stringify(40),
      '__WEBPACK_DEFINE__.DEMO_DATA': JSON.stringify(generation()),
      '__WEBPACK_DEFINE__.DEFAULT_DATA_COUNT': JSON.stringify({ MIN: 100, MAX: 500 }),

      '__WEBPACK_DEFINE__.DATA_COUNT': JSON.stringify({
        [600]: { MIN: 20, MAX: 100 },
        [960]: { MIN: 40, MAX: 200 },
        [1280]: { MIN: 60, MAX: 300 },
        [1920]: { MIN: 80, MAX: 400 }
      })
    })
  ],
  module: {
    rules: [{
      test: /\.(ts|tsx)$/,
      exclude: [/(node_modules)/],
      use: [{
        loader: 'babel-loader',
        ...(mode !== 'production' && {
          options: {
            plugins: ['react-refresh/babel']
          }
        })
      }]
    }]
  }
});