import React, { useState, useMemo, useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';
import numeral from 'numeral';
import { generate as uuid } from 'shortid';

import GlobalTheme from '@utils/global-theme';


//* Definition
export const CANDLESTICK_PROP_TYPES = {
  DATA: {
    open: PropTypes.number.isRequired,
    close: PropTypes.number.isRequired,
    high: PropTypes.number.isRequired,
    low: PropTypes.number.isRequired,
    volume: PropTypes.number.isRequired,

    ts: PropTypes.exact({
      primary: PropTypes.string.isRequired,
      secondary: PropTypes.string.isRequired
    }).isRequired
  },
  OPTIONS: {
    bearColor: PropTypes.exact({ //* 漲的顏色
      default: PropTypes.string,
      highlight: PropTypes.string
    }),
    bullColor: PropTypes.exact({ //* 跌的顏色
      default: PropTypes.string,
      highlight: PropTypes.string
    }),
    maxCandleWidth: PropTypes.number, //* 最大寬度
    gridColor: PropTypes.string, //* Price Grid 的線條顏色
    minGridScale: PropTypes.number, //* 最小的 Price Grid 間隔值
    padding: PropTypes.number, //* Y 軸上下空白高度 px
    symbol: PropTypes.string, //* 圖表浮水印
    timeSeries: PropTypes.string.isRequired,
    timeZone: PropTypes.string.isRequired, //* 時間區域
    zoomedCount: PropTypes.number //* Zoom 改變時, 資料增加/減少的數量
  }
};

export namespace CandlestickHooks {
  enum TimeSeriesEnum { minutely, hourly, daily, monthly };

  export type ChartOptions = PropTypes.InferProps<typeof CANDLESTICK_PROP_TYPES['OPTIONS']>;
  export type CandlestickData = PropTypes.InferProps<typeof CANDLESTICK_PROP_TYPES['DATA']>;
  export type Point = { x: number; y: number; };
  export type Size = { height: number; width: number; };
  export type TimeSeries = keyof typeof TimeSeriesEnum;

  export interface CandlestickOptions extends Pick<ChartOptions, 'bearColor' | 'bullColor' | 'gridColor' | 'minGridScale' | 'padding' | 'symbol' | 'timeSeries'> {
    ctx: CanvasRenderingContext2D;
    breakpoint: 'xs' | 'sm' | 'md' | 'lg' | 'xl';
    innerSize: Size;
    outterSize: Size;
    seriesWidth: number;
    sliceCount: number;

    setZoom: (type: 'zoomin' | 'zoomout') => void;
  };

  export type DrawerHooks = [Point & { data: CandlestickData; }, {
    onHighlight: (e: React.MouseEvent<HTMLCanvasElement>) => void;
    onDraw: () => void;
    onZoomChange: CandlestickOptions['setZoom'];
  }];

  export interface SeriesOptions {
    color: { default: string; highlight: string; };
    coordinate: Point;
    height: number;
    ts?: CandlestickData['ts'];
    wick: [number, number];
    width: number;
  };

  export interface WebpackDefine {
    DEFAULT_PADDING_LEFT: number;
    DEFAULT_DATA_COUNT: { MIN: number; MAX: number };

    DATA_COUNT: {
      [key: number]: { MIN: number; MAX: number };
      FULL: { MIN: number; MAX: number };
    }
  }

  export type CandleData = SeriesOptions & { data: CandlestickData; };

  export type CalcCoordinate = {
    forGridEach: (eachFn: (marker: string, coordinateY: number) => void) => void;
    getCoordinate: (scale: CandlestickHooks.Point, typeY: 'scale' | 'price') => CandlestickHooks.Point;
    getPosition: (type: 'absolute' | 'fixed', series: CandlestickHooks.SeriesOptions) => CandlestickHooks.Point;
    getPriceCoordinateY: (price: number) => number;
  };
}

declare const __WEBPACK_DEFINE__: CandlestickHooks.WebpackDefine;


//* Custom Hooks
const useCalcCoordinate = (() => {
  const getCoordinateX = ({ innerSize }: CandlestickHooks.CandlestickOptions, scaleX: number) => (
    (innerSize.width * scaleX) + __WEBPACK_DEFINE__.DEFAULT_PADDING_LEFT
  );

  const getCoordinateY = ({ innerSize, padding }: CandlestickHooks.CandlestickOptions, scaleY: number) => (
    (innerSize.height * scaleY) + padding
  );

  const getMaxAndMinPrice = (scale: number, data: CandlestickHooks.CandlestickData[]) => {
    const prices = data.reduce<number[]>(
      (result, record) => (
        Object.entries(record).reduce(
          (list, [field, value]) => (
            field === 'ts' || field === 'volume'
              ? list
              : list.concat(value as number)
          ),
          result
        )
      ),
      []
    );

    return {
      max: Math.ceil(Math.max(...prices) / scale) * scale,
      min: Math.floor(Math.min(...prices) / scale) * scale
    };
  };

  return (options: CandlestickHooks.CandlestickOptions, data: CandlestickHooks.CandlestickData[]): CandlestickHooks.CalcCoordinate => {
    const extremum = useMemo(() => {
      const { max, min } = getMaxAndMinPrice(options.minGridScale, data);

      return {
        diff: max - min,
        height: Math.max(options.minGridScale, (max - min) / 4),
        max,
        min
      };
    }, [options.minGridScale, data]);

    return {
      forGridEach: useCallback((eachFn) => {
        const base = extremum.diff / extremum.height;
        let { min: start } = extremum;
        let counter = 0;

        while (start <= extremum.max) {
          eachFn(numeral(start).format('0,0.00'), getCoordinateY(options, 1 - (counter / base)));
          start += extremum.height;
          counter++;
        }
      }, [options, extremum]),

      getCoordinate: useCallback(({ x: scaleX, y: scaleY }, typeY) => ({
        x: getCoordinateX(options, scaleX),
        y: getCoordinateY(options, typeY === 'scale' ? scaleY : (extremum.max - scaleY) / extremum.diff)
      }), [options, extremum]),

      getPosition: useCallback((type: 'absolute' | 'fixed', { wick, width, coordinate }) => {
        const { left, top } = options.ctx.canvas.getBoundingClientRect();
        const result: CandlestickHooks.Point = { x: coordinate.x + (width / 2), y: wick[0] };

        return type === 'absolute' ? result : { x: result.x + left, y: result.y + top };
      }, [options.ctx]),

      getPriceCoordinateY: useCallback((price: number) => (
        getCoordinateY(options, (extremum.max - price) / extremum.diff)
      ), [options, extremum])
    };
  };
})();

const useOptions = (defaultOptions: CandlestickHooks.ChartOptions): CandlestickHooks.ChartOptions => (
  useMemo(() => {
    const { bearColor, bullColor, maxCandleWidth, gridColor, minGridScale, padding, zoomedCount, ...options } = defaultOptions;

    return {
      bearColor: {
        default: bearColor?.default || GlobalTheme.chart.bear.default,
        highlight: bearColor?.highlight || GlobalTheme.chart.bear.highlight
      },
      bullColor: {
        default: bullColor?.default || GlobalTheme.chart.bull.default,
        highlight: bullColor?.highlight || GlobalTheme.chart.bull.highlight
      },
      maxCandleWidth: maxCandleWidth || 5,
      gridColor: gridColor || GlobalTheme.chart.grid,
      minGridScale: minGridScale || 5,
      padding: padding || 10,
      zoomedCount: 20,
      ...options
    };
  }, [JSON.stringify(defaultOptions)])
);

const useDrawCandleWick = (options: CandlestickHooks.CandlestickOptions) => (
  useCallback(({ ts, color: { 'default': normal, highlight }, coordinate, width, height, wick }: CandlestickHooks.SeriesOptions, isHighlight: boolean = false) => {
    const { ctx, gridColor, outterSize } = options;
    const color = isHighlight ? highlight : normal;

    if (isHighlight) {
      ctx.save();
      ctx.strokeStyle = gridColor;
      ctx.beginPath();
      ctx.moveTo(coordinate.x + (width / 2), 0);
      ctx.lineTo(coordinate.x + (width / 2), outterSize.height);
      ctx.stroke();
      ctx.restore();
    }

    //* Wick - 直接從 high 到 low
    ctx.save();
    ctx.strokeStyle = color;
    ctx.beginPath();
    ctx.moveTo(coordinate.x + (width / 2), wick[0]);
    ctx.lineTo(coordinate.x + (width / 2), wick[1]);
    ctx.stroke();
    ctx.restore();

    //* Candle - 可以覆蓋掉部分 wick
    ctx.save();
    ctx.fillStyle = color;
    ctx.fillRect(coordinate.x, coordinate.y, width, height);
    ctx.restore();

    if (ts) {
      ctx.save();
      ctx.fillStyle = GlobalTheme.chart.marker;
      ctx.font = GlobalTheme.chart.font.marker;
      ctx.textAlign = 'right';
      ctx.fillText(ts.primary, coordinate.x, outterSize.height - 15, 40);
      ctx.restore();

      ctx.save();
      ctx.fillStyle = GlobalTheme.chart.marker;
      ctx.font = GlobalTheme.chart.font.marker;
      ctx.textAlign = 'right';
      ctx.fillText(ts.secondary, coordinate.x, outterSize.height - 5, 40);
      ctx.restore();
    }
  }, [options])
);

//* 繪製 Grid Line & Price Marker
const useDrawGridLines = (options: CandlestickHooks.CandlestickOptions) => (
  useCallback((marker: string, coordinateY: number) => {
    const { ctx, gridColor, outterSize } = options;
    const markerWidth = __WEBPACK_DEFINE__.DEFAULT_PADDING_LEFT - 5;

    //* Grid Line
    ctx.save();
    ctx.strokeStyle = gridColor;
    ctx.beginPath();
    ctx.moveTo(0, coordinateY);
    ctx.lineTo(outterSize.width, coordinateY);
    ctx.stroke();
    ctx.restore();

    //* Price Marker
    ctx.save();
    ctx.fillStyle = GlobalTheme.chart.marker;
    ctx.font = GlobalTheme.chart.font.marker;
    ctx.textAlign = 'right';
    ctx.fillText(marker, markerWidth, coordinateY - 2, markerWidth);
    ctx.restore();
  }, [options])
);

const useDrawSymbolText = (symbol: string) => (
  useCallback(({ ctx, outterSize }: CandlestickHooks.CandlestickOptions) => {
    if (symbol) {
      ctx.save();
      ctx.fillStyle = GlobalTheme.chart.symbol;
      ctx.font = GlobalTheme.chart.font.symbol;
      ctx.textAlign = 'center';
      ctx.fillText(symbol, outterSize.width / 2, outterSize.height / 2 + 25);
      ctx.restore();
    }
  }, [symbol])
);

export default (options: CandlestickHooks.CandlestickOptions, defaultData: CandlestickHooks.CandlestickData[]): CandlestickHooks.DrawerHooks => {
  const data = useMemo(() => defaultData.slice(0, options.sliceCount), [defaultData, options.sliceCount]);
  const { forGridEach, getCoordinate, getPosition, getPriceCoordinateY } = useCalcCoordinate(options, data);
  const [candles, setCandles] = useState<CandlestickHooks.CandleData[]>([]);
  const [selected, setSelected] = useState<CandlestickHooks.CandleData>(null);

  const drawCandleWick = useDrawCandleWick(options);
  const drawGridLines = useDrawGridLines(options);
  const drawSymbolText = useDrawSymbolText(options.symbol);

  return [
    useMemo(() => (
      !selected ? null : { ...getPosition('fixed', selected), data: selected.data }
    ), [selected, getPosition]),

    {
      onHighlight: useCallback((e: React.MouseEvent<HTMLCanvasElement>) => {
        const { ctx, innerSize, sliceCount } = options;

        if (ctx) {
          const { left, top } = ctx.canvas.getBoundingClientRect();
          const pointX = e.clientX - left - __WEBPACK_DEFINE__.DEFAULT_PADDING_LEFT;
          const pointY = e.clientY - top;
          const candle = candles[Math.floor(sliceCount * ((innerSize.width - pointX) / innerSize.width))];
          const [min, max] = candle?.wick || [];

          if (selected) {
            drawCandleWick(selected);
            setSelected(null);
          }

          if (max >= pointY && min <= pointY) {
            const { data, color, ...dataOpt } = candle;
            const isBull = data.close > data.open;
            const target: CandlestickHooks.SeriesOptions = { ...dataOpt, color: options[isBull ? 'bullColor' : 'bearColor'] };

            drawCandleWick(target, true);
            setSelected(candle);
          }
        }
      }, [candles, selected]),

      onDraw: useCallback(() => {
        const { ctx, breakpoint } = options;

        if (ctx) {
          const ceil = Math.ceil(options.sliceCount / (breakpoint === 'xs' ? 4 : 8));
          const { data: { ts = null } = {} } = selected || {};

          ctx.save();
          ctx.clearRect(0, 0, options.outterSize.width, options.outterSize.height);
          ctx.restore();

          drawSymbolText(options);
          forGridEach(drawGridLines);
          setSelected(null);

          setCandles(
            data.map((rowData, i) => {
              const isBull = rowData.close > rowData.open;
              const price = { close: getPriceCoordinateY(rowData.close), open: getPriceCoordinateY(rowData.open) };

              const series: CandlestickHooks.SeriesOptions = {
                color: options[isBull ? 'bullColor' : 'bearColor'],
                coordinate: getCoordinate({ x: 1 - ((i + 1) / options.sliceCount), y: Math.max(rowData.open, rowData.close) }, 'price'),
                height: Math.max(1, Math.abs(price.open - price.close)),  //* 高度至少為 1
                wick: [getPriceCoordinateY(rowData.high), getPriceCoordinateY(rowData.low)],
                width: options.seriesWidth,

                ...((i % ceil === 0 || i === (data.length - 1)) && {
                  ts: rowData.ts
                })
              };

              if (JSON.stringify(ts) === JSON.stringify(rowData.ts)) {
                setSelected({ ...series, data: rowData });
                drawCandleWick(series, true);
              } else {
                drawCandleWick(series);
              }

              return { ...series, data: rowData };
            })
          );
        }
      }, [forGridEach, options, data, JSON.stringify(selected)]),

      onZoomChange: options.setZoom
    }
  ];
};

export const useCanvasChart = (
  defaultOptions: CandlestickHooks.ChartOptions,
  total: number
): [(el: HTMLCanvasElement) => void, string, CandlestickHooks.CandlestickOptions] => {
  const [canvasKey, setCanvasKey] = useState(uuid());
  const [sliceCount, setSliceCount] = useState(0);
  const [el, setEl] = useState<HTMLCanvasElement>(null);
  const options = useOptions(defaultOptions);

  //* window resize 時直接產生新的 key, 讓 canvas 重新 mount 到畫面上
  useEffect(() => {
    const fn = () => setCanvasKey(uuid());

    window.addEventListener('resize', fn);

    return () => window.removeEventListener('resize', fn);
  }, [el, total]);

  return [
    useCallback(setEl, []),
    canvasKey,

    useMemo(() => {
      const { width = 0, height = 0 } = el?.getBoundingClientRect() || {};
      const breakpoints = Object.entries(__WEBPACK_DEFINE__.DATA_COUNT);
      const bpIndex = breakpoints.findIndex(([maxWidth]) => Number.parseFloat(maxWidth) > width);
      const breakpoint = bpIndex === 0 ? 'xs' : bpIndex === 1 ? 'sm' : bpIndex === 2 ? 'md' : bpIndex === 3 ? 'lg' : 'xl';
      const { MAX: max, MIN: min } = breakpoints[bpIndex][1] || __WEBPACK_DEFINE__.DEFAULT_DATA_COUNT;
      const count = (sliceCount && Math.max(min, Math.min(max, total, sliceCount))) || max;

      const innerSize = {
        height: Math.max(0, height - (options.padding * 2) - 15), //* 15 是預留給 timestamp 使用
        width: width - __WEBPACK_DEFINE__.DEFAULT_PADDING_LEFT
      };

      if (el) {
        el.width = width;
        el.height = height;
      }

      return {
        ...options,

        ctx: el?.getContext('2d'),
        breakpoint,
        innerSize,
        outterSize: { height, width },
        seriesWidth: Math.min(options.maxCandleWidth, (innerSize.width / count) - 1),
        sliceCount: count,

        setZoom: (type) => (
          setSliceCount(Math.max(min, Math.min(max, total, (sliceCount || max) + (options.zoomedCount * (type === 'zoomin' ? -1 : 1)))))
        )
      };
    }, [el, sliceCount, options, total])
  ];
};
