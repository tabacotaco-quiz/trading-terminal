namespace GlobalThemeDefinition {
  type GradientColors = { light: string; main: string; dark: string; };

  export interface Options {
    fontFamily: string;
    primary: GradientColors;
    secondary: GradientColors;
    info: GradientColors;
    success: GradientColors;
    warning: GradientColors;
    error: GradientColors;
    spacing: (...multiples: number[]) => string;

    background: {
      default: string;
      paper: string;
    };
    text: {
      primary: string;
      secondary: string;
    };
    chart: {
      grid: string;
      marker: string;
      stroke: string;
      symbol: string;

      font: {
        marker: string;
        symbol: string;
      },
      bear: {
        default: string;
        highlight: string;
      },
      bull: {
        default: string;
        highlight: string;
      }
    }
  }
}

export type GlobalThemeOptions = GlobalThemeDefinition.Options;

const theme: GlobalThemeDefinition.Options = {
  fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
  spacing: (...multiples: number[]) => multiples.map((multiple) => `${multiple * 8}px`).join(' '),

  background: {
    default: '#303030',
    paper: '#424242'
  },
  text: {
    primary: '#fff',
    secondary: 'rgba(255, 255, 255, 0.7)'
  },
  primary: {
    light: '#e1bee7',
    main: '#ba68c8',
    dark: '#9c27b0'
  },
  secondary: {
    light: '#ffecb3',
    main: '#ffd54f',
    dark: '#ffc107'
  },
  info: {
    light: '#b3e5fc',
    main: '#4fc3f7',
    dark: '#03a9f4'
  },
  success: {
    light: '#b2dfdb',
    main: '#4db6ac',
    dark: '#009688'
  },
  warning: {
    light: '#fff176',
    main: '#ffeb3b',
    dark: '#f9a825'
  },
  error: {
    light: '#ffccbc',
    main: '#ff8a65',
    dark: '#ff5722'
  },
  chart: {
    grid: '#dbdbdb',
    marker: '#808080',
    stroke: '#dbdbdb',
    symbol: 'rgba(235, 239, 244, 0.2)',

    bear: {
      default: 'rgb(251, 108, 100, 0.6)',
      highlight: '#f91c10'
    },
    bull: {
      default: 'rgb(61, 146, 250, 0.6)',
      highlight: '#066fef'
    },
    font: {
      marker: 'bold 8px Arial',
      symbol: 'bold 100px Arial'
    }
  }
};

export default theme;
