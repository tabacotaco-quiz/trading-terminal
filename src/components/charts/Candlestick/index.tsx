import React, { useImperativeHandle, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import numeral from 'numeral';

import Tooltip from '@components/layouts/Tooltip';
import { GlobalThemeOptions } from '@utils/global-theme';

import useCandlestickDrawer, { CANDLESTICK_PROP_TYPES, CandlestickHooks, useCanvasChart } from './useCandlestick';


//* Definitions
const PROP_TYPES = {
  ...CANDLESTICK_PROP_TYPES.OPTIONS,

  action: PropTypes.element,
  className: PropTypes.string,
  information: PropTypes.string,
  outputSize: PropTypes.string, //? 暫時不知道變數目的
  onTimeSeriesChange: PropTypes.func.isRequired,

  data: PropTypes.arrayOf(
    PropTypes.exact(CANDLESTICK_PROP_TYPES.DATA).isRequired
  )
};

namespace CandlestickComponent {
  export interface Props extends Omit<PropTypes.InferProps<typeof PROP_TYPES>, 'data' | 'timeSeries' | 'onTimeSeriesChange'> {
    data: CandlestickHooks.CandlestickData[];
    timeSeries: CandlestickHooks.TimeSeries;
    onTimeSeriesChange?: (newTimeSeries: CandlestickHooks.TimeSeries) => void;
  }
}

export type CandlestickTimeSeries = CandlestickComponent.Props['timeSeries'];

export type CandlestickDataType = PropTypes.InferProps<typeof CANDLESTICK_PROP_TYPES['DATA']>;

export type CandlestickRef = {
  setZoom: CandlestickHooks.CandlestickOptions['setZoom'];
};


//* Components
const Container = styled('div')`
  ${({ theme }: { theme: GlobalThemeOptions; }) => `
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    width: 100%;

    & > *[role=chart] {
      height: 100%;
      flex-grow: 1;
      flex-basis: 100%;
      max-width: 100%;
    }

    & > *[role=action] {
      flex-grow: 1;
      flex-basis: ${theme.spacing(6)};
      width: ${theme.spacing(6)};
      margin-right: ${theme.spacing(1)};

      & + *[role=chart] {
        position: relative;
        flex-basis: calc(100% - ${theme.spacing(7)});
        max-width: calc(100% - ${theme.spacing(7)});
        max-height: 50vh;
        min-height: ${theme.spacing(50)};
      }
    }

    & > *[role=info] {
      flex-basis: 100%;
      max-width: 100%;
    }
  `}
`;

const Caption = styled('caption')`
  ${({ theme }: { theme: GlobalThemeOptions; }) => `
    color: ${theme.chart.symbol};
    text-align: center;
    text-transform: capitalize;
  `}
`;

const Chart = styled('canvas')`
  ${({ theme }: { theme: GlobalThemeOptions; }) => `
    background: ${theme.background.paper};
    border-radius: ${theme.spacing(0.5)};
    width: 100%;
    height: 100%;
  `}
`;

const Text = styled('p')`
  font-size: 10px;
  line-height: 1.2;
`;

const Toolbar = styled('nav')`
  ${({ theme }: { theme: GlobalThemeOptions; }) => `
    background: ${theme.background.paper};
    border-radius: ${theme.spacing(0.5)};
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: ${theme.spacing(1, 0)};
    overflow: hidden auto;

    & > * + * {
      margin-top: ${theme.spacing(1)}
    }

    & > *[role=divider] {
      width: 100%;
      border: 1px solid ${theme.chart.grid};
    }

    & > button[role=series] {
      cursor: pointer;
      display: flex;
      justify-content: center;
      align-items: center;
      width: 100%;
      height: ${theme.spacing(4)};
      text-transform: capitalize;
    }
  `}
`;

const Candlestick = React.forwardRef<CandlestickRef, CandlestickComponent.Props>(({
  action,
  bearColor,
  bullColor,
  maxCandleWidth,
  className,
  data,
  gridColor,
  minGridScale,
  information,
  padding,
  symbol,
  timeSeries,
  timeZone,

  onTimeSeriesChange
}, ref) => {
  const [canvasRef, chartKey, options] = useCanvasChart({ bearColor, bullColor, maxCandleWidth, gridColor, minGridScale, padding, symbol, timeSeries, timeZone }, data.length);
  const [tooltip, { onHighlight, onDraw, onZoomChange }] = useCandlestickDrawer(options, data);

  useImperativeHandle(ref, () => ({
    setZoom: onZoomChange
  }));

  useEffect(() => {
    onDraw();
  }, [onDraw]);

  return (
    <Container role="container" className={className}>
      <Toolbar role="action">
        {action && (
          <>
            {action}
            <hr role="divider" />
          </>
        )}

        {['minutely', 'hourly', 'daily', 'monthly'].map((type) => (
          <button role="series" disabled={type === timeSeries} onClick={() => onTimeSeriesChange(type as CandlestickHooks.TimeSeries)}>
            {type}
          </button>
        ))}
      </Toolbar>

      <div role="chart">
        <Chart
          key={chartKey}
          ref={canvasRef}
          onClick={onHighlight}
          onMouseMove={onHighlight}
          onWheel={({ deltaY }: { deltaY: number; }) => options.setZoom(deltaY < 0 ? 'zoomin' : 'zoomout')}
        />

        <Tooltip position="fixed" open={Boolean(tooltip)} x={tooltip?.x} y={tooltip?.y}>
          <Text>{tooltip?.data.ts.secondary}&nbsp;{tooltip?.data.ts.primary}</Text>
          <Text>Open: {numeral(tooltip?.data.open).format('0,0.00')}</Text>
          <Text>High: {numeral(tooltip?.data.high).format('0,0.00')}</Text>
          <Text>Low: {numeral(tooltip?.data.low).format('0,0.00')}</Text>
          <Text>Close: {numeral(tooltip?.data.close).format('0,0.00')}</Text>
          <Text>Volume: {numeral(tooltip?.data.volume).format('0,0.00')}</Text>
        </Tooltip>
      </div>

      {information && (
        <Caption role="info">
          {information}
        </Caption>
      )}
    </Container>
  );
});

Candlestick.displayName = 'Candlestick';

export default Candlestick;
