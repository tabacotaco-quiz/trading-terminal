const VARIABLE_PERCENTAGE = 0.03; //* 變動比例 (產生測試資料用)
const SERIES = ['minutely', 'hourly', 'daily', 'monthly'];

const getPreviousTs = (date) => {
  const weekDay = date.getUTCDay();

  if (weekDay > 0 && weekDay < 6) {
    date.setMinutes(date.getMinutes() - 1);
    const hours = date.getUTCHours();

    if (hours <= 6) {
      return new Date(`${date.toISOString().substring(0, 16)}:00.000Z`);
    }

    return getPreviousTs(new Date(`${date.toISOString().substring(0, 10)}T06:01:00.000Z`));
  }

  date.setDate(date.getDate() - (weekDay === 0 ? 2 : 1));

  return new Date(`${date.toISOString().substring(0, 10)}T06:01:00.000Z`);
}

const getRandomBoolean = () => (
  (Math.round(Math.random() * 1000) % 2) === 1
);

const getDiceNumber = (limit, positive = getRandomBoolean()) => (
  Number.parseFloat((Math.random() * limit).toFixed(2)) * (positive ? 1 : -1)
);

const getIntervalPrices = (list) => {
  const { prices, volume } = list.reduce(
    ({ prices: p, volume: v }, { open, close, high, low, volume }) => ({
      prices: p.concat(open, close, high, low),
      volume: v + volume
    }),
    { prices: [], volume: 0 }
  );

  return {
    max: Math.max(...prices),
    min: Math.min(...prices),
    volume
  };
};

const getPrices = (cardinal) => {
  const direction = getRandomBoolean();

  return Array
    .from({ length: 4 })
    .map(() => {
      const value = cardinal * (1 + ((Math.round(getDiceNumber(VARIABLE_PERCENTAGE * 1000)) / 1000) * (getRandomBoolean() ? 1 : -1)));

      return Number.parseFloat(value.toFixed(2));
    })
    .sort((n1, n2) => (direction ? (n1 - n2) : (n2 - n1)));
};

const getTransformData = ({ ts, open, close, high, low, volume }, primary, secondary) => ({
  open,
  close,
  high,
  low,
  volume,
  ts: {
    primary: `${ts.substring(...primary.slice(0, 2))}${primary[2] || ''}`,
    secondary: ts.substring(...secondary)
  }
});

const generator = () => {
  const groups = {};
  const data = [];

  for (let i = 0; i < 14400; i++) {
    const prevData = data[data.length - 1];
    const ts = getPreviousTs(prevData ? new Date(prevData.ts) : new Date());
    const cardinal = Math.ceil(i === 0 ? 200 : ((prevData.low + prevData.close) * 0.5 + getDiceNumber(2, true)));
    const volume = Math.round(getDiceNumber(300000, true));
    const [num1, close, open, num2] = getPrices(cardinal);

    const newData = {
      ts: ts.toISOString(),
      cardinal,
      high: Math.max(num1, num2),
      low: Math.min(num1, num2),
      open,
      close,
      volume
    };

    console.log(cardinal);
    data.push(newData);

    SERIES.forEach((series) => {
      const { [series]: result = {} } = groups;
      const { ts } = newData;

      switch (series) {
        case 'hourly': { //* yyyy-MM-ddThh
          const key = ts.substring(0, 13);
          const { [key]: list = [] } = result;

          groups[series] = { ...result, [key]: list.concat(getTransformData(newData, [11, 13, ':00'], [0, 10])) };
          break;
        }
        case 'daily': { //* yyyy-MM-dd
          const key = ts.substring(0, 10);
          const { [key]: list = [] } = result;

          groups[series] = { ...result, [key]: list.concat(getTransformData(newData, [8, 10], [0, 7])) };
          break;
        }
        case 'monthly': { //* yyyy-MM
          const key = ts.substring(0, 7);
          const { [key]: list = [] } = result;

          groups[series] = { ...result, [key]: list.concat(getTransformData(newData, [5, 7], [0, 4])) };
          break;
        }
        default: { //* yyyy-MM-ddThh:mm
          const key = ts.substring(0, 16);

          groups[series] = { ...result, [key]: [getTransformData(newData, [11, 16], [0, 10])] };
          break;
        }
      }
    })
  }

  return Object.entries(groups).reduce(
    (result, [series, groups], i) => ({
      ...result,
      [series]: Object.values(groups).map((list) => {
        const [{ ts }] = list;
        const { max: high, min: low, volume } = getIntervalPrices(list);

        return {
          ts,
          open: list[0].open,
          close: list[list.length - 1].close,
          high,
          low,
          volume
        };
      })
    }),
    {}
  );
};

module.exports = generator;
