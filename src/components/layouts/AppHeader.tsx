import React from 'react';
import PropTypes from 'prop-types';
import styled, { useTheme } from 'styled-components';
import { NavLink } from 'react-router-dom';

import { Activity as ActivityIcon } from 'react-feather';

import { GlobalThemeOptions } from '@utils/global-theme';


//* Prop Types
const PROP_TYPES = {
  className: PropTypes.string,
  children: PropTypes.node
};


//* Components
const Header = styled('header')`
  ${({ theme }: { theme: GlobalThemeOptions; }) => `
    display: flex;
    flex-direction: row;
    align-items: center;
    background: ${theme.background.paper};
    min-height: ${theme.spacing(6)};
    padding: ${theme.spacing(1, 3)};
  `}
`;

const TitleLink = styled(NavLink)`
  ${({ theme }: { theme: GlobalThemeOptions; }) => `
    display: flex;
    flex-direction: row;
    align-items: center;
    color: ${theme.text.primary};
    font-size: 1.5rem;
    font-weight: 600;
    text-decoration: none;

    & > svg[role=icon] {
      margin-right: ${theme.spacing(1)}
    }
  `}
`;

const AppHeader: React.FC<PropTypes.InferProps<typeof PROP_TYPES>> = ({ className, children }) => {
  const theme = useTheme() as GlobalThemeOptions;

  return (
    <Header role="toolbar" className={className}>
      <TitleLink role="title" to="/">
        <ActivityIcon role="icon" color={theme.primary.main} />

        Trading Terminal
      </TitleLink>

      {children}
    </Header>
  );
};

AppHeader.displayName = 'AppHeader';
AppHeader.propTypes = PROP_TYPES;

export default AppHeader;
