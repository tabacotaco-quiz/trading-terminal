import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { GlobalThemeOptions } from '@utils/global-theme';


//* Definitions
const PROP_TYPES = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  open: PropTypes.bool.isRequired,
  position: PropTypes.oneOf<'absolute' | 'fixed'>(['absolute', 'fixed']).isRequired,
  x: PropTypes.number,
  y: PropTypes.number
};

//* Components
const Container = styled('div')`
  ${({ theme, position, top, left }: { theme: GlobalThemeOptions, position: 'absolute' | 'fixed', top: number; left: number; }) => `
    position: ${position};
    top: ${top}px;
    left: ${left}px;
    display: block;
    transform: translate(-50%, calc(-100% - ${theme.spacing(1)}));
    width: fit-content;

    & > div[role=content] {
      background: gray;
      border-radius: ${theme.spacing(0.5)};
      padding: ${theme.spacing(1, 1.5)};
    }

    & > span[role=arrow] {
      position: absolute;
      bottom: 0;
      left: 50%;
      transform: translate(-50%);
      width: ${theme.spacing(3)};
      height: ${theme.spacing(1)};
      margin-bottom: ${theme.spacing(-1)};

      &::after {
        border-color: gray transparent transparent;
        border-width: ${theme.spacing(1, 1, 0, 1)};;
        border-style: solid;
        width: 0;
        height: 0;
        margin: auto;
        content: "";
        display: block;
      }
    }
  `}
`;

const Tooltip: React.FC<PropTypes.InferProps<typeof PROP_TYPES>> = ({ children, className, open, position, x, y }) => (
  open && (
    <Container className={className} position={position} top={y} left={x}>
      <div role="content">
        {children}
      </div>

      <span role="arrow" />
    </Container>
  )
);

Tooltip.displayName = 'Tooltip';
Tooltip.propTypes = PROP_TYPES;

export default Tooltip;
