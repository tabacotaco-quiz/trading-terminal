import React from 'react';
import styled, { ThemeProvider as StyledProvider, createGlobalStyle } from 'styled-components';
import { HashRouter, Routes, Route } from 'react-router-dom';
import { createRoot } from 'react-dom/client';

import routes from './pages';
import theme from './utils/global-theme';


const GlobalStyle = createGlobalStyle`
  body {
    background: ${theme.background.default}; //* styles - background color
    color: ${theme.text.primary}; //* styles - default text color
    font-family: ${theme.fontFamily};
    padding: 0;
    margin: 0;

    & > #app {
      display: flex;
      flex-direction: column;
      height: 100vh;
      width: 100vw;
      overflow: hidden auto;
    }
  }
`;

createRoot(document.getElementById('app'))
  .render(
    <StyledProvider theme={theme}>
      <GlobalStyle />

      <HashRouter basename="/">
        <Routes>
          {routes.map((routeProps) => (
            <Route key={routeProps.path} {...routeProps} />
          ))}
        </Routes>
      </HashRouter>
    </StyledProvider>
  );
